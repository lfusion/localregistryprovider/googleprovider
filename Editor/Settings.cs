/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider
{
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;

	[FilePath("ProjectSettings/LFusion/GoogleProviderSettings.asset", FilePathAttribute.Location.ProjectFolder)]
	public class Settings : ScriptableSingleton<Settings>
	{
		#region Fields
		[SerializeField] private List<string> m_enabledPackages = new List<string>();
		#endregion Fields

		#region Properties
		public List<string> EnabledPackages { get => m_enabledPackages; }
		#endregion Properties

		#region Methods
		#region MonoBehaviours
		private void OnEnable()
		{
			hideFlags &= ~HideFlags.NotEditable;
		}

		private void OnDisable()
		{
			Save();
		}
		#endregion MonoBehaviours

		/// <summary>
		/// Save the timeline project settings file in the project directory.
		/// </summary>
		public void Save()
		{
			Save(true);
		}

		public static SerializedObject GetSerialized()
		{
			return new SerializedObject(instance);
		}
		#endregion Methods
	}
}
