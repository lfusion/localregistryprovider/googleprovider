/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider
{
	/// <summary>
	/// Contain Google Provider package informations
	/// </summary>
	public static class Constants
	{
		public const string PackageName = "com.lfusion.localregistryprovider.googleprovider";

		public const string PackageDisplayedName = "Google Provider";
		public const string PackageShortDisplayedName = PackageDisplayedName;
		public const string Description = 
@"Fetchs Google's packages available from:
 - https://developers.google.com/unity/archive
 - https://github.com/android/tuningfork/releases";
	}
}