/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider.UI
{
	using LFUI = LFusion.LocalRegistryProvider.UI;
	using System;
	using System.Collections.Generic;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;
	using System.Linq;
	using System.Threading.Tasks;
	using System.Diagnostics;

	/// <summary>
	/// VisualElement to edit enabledpackagesList
	/// </summary>
	public class PackageListToggle : VisualElement
	{
		#region Constantes
		private const string LABEL_TEXT = "{0} packages versions enabled";
		private const string GOOGLE_PACKAGE_PREFIX = "com.google.";
		private const string SCRAPED_PACKAGE_UXML_FILENAME = "ScrapedPackage.uxml";
		private const int MAX_DURATION_BEFORE_YIELD = 10;
		#endregion Constantes

		#region Fields
		private List<string> m_value;
		private List<ScrapedPackageInfo> m_packageInfos;
		private Toolbar m_toolbar = null;
		private VisualElement m_packagesContainer = null;
		private Label m_label = null;
		private ToolbarButton m_toolbarBtn1 = null;
		private ToolbarButton m_toolbarBtn2 = null;
		private Stopwatch m_execStopwatch = new Stopwatch();
		private LFUI.BusyOverlay m_busyOverlay = null;
		#endregion Fields

		#region Events
		public event Action<string, bool> ValueChanged;
		public event Action ToolbarButton1Clicked;
		public event Action ToolbarButton2Clicked;
		#endregion Events

		#region Properties
		public List<ScrapedPackageInfo> PackageInfos { get => m_packageInfos; set => m_packageInfos = value; }
		public List<string> Value { get => m_value; set => m_value = value; }
		public string ToolbarButton1Text
		{ 
			get
			{
				return m_toolbarBtn1.text;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					m_toolbarBtn1.visible = false;
				}
				else
				{
					m_toolbarBtn1.visible = true;
				}

				m_toolbarBtn1.text = value;
			}
		}
		public string ToolbarButton2Text
		{
			get
			{
				return m_toolbarBtn2.text;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					m_toolbarBtn2.visible = false;
				}
				else
				{
					m_toolbarBtn2.visible = true;
				}

				m_toolbarBtn2.text = value;
			}
		}
		public bool ToolbarButton1Enabled
		{
			get => m_toolbarBtn1.enabledSelf;
			set => m_toolbarBtn1.SetEnabled(value);
		}
		public bool ToolbarButton2Enabled
		{
			get => m_toolbarBtn2.enabledSelf;
			set => m_toolbarBtn2.SetEnabled(value);
		}
		public override VisualElement contentContainer => m_toolbar;
		#endregion Properties

		#region Methods
		public PackageListToggle()
		{
			m_toolbar = new Toolbar();
			hierarchy.Add(m_toolbar);

			m_packagesContainer = new VisualElement();
			hierarchy.Add(m_packagesContainer);

			m_label = new Label();
			m_toolbar.Add(m_label);

			ToolbarSpacer toolbarSpacer = new ToolbarSpacer();
			toolbarSpacer.style.flexGrow = 1;
			m_toolbar.Add(toolbarSpacer);

			m_toolbarBtn1 = new ToolbarButton();
			m_toolbarBtn1.text = "";
			m_toolbarBtn1.visible = false;
			m_toolbarBtn1.clicked += OnToolbarBtn1Clicked;
			m_toolbarBtn1.style.borderRightWidth = 0;
			m_toolbar.Add(m_toolbarBtn1);

			m_toolbarBtn2 = new ToolbarButton();
			m_toolbarBtn2.text = "";
			m_toolbarBtn2.visible = false;
			m_toolbarBtn2.clicked += OnToolbarBtn2Clicked;
			m_toolbarBtn2.style.borderRightWidth = 0;
			m_toolbar.Add(m_toolbarBtn2);

			m_packagesContainer.style.flexDirection = FlexDirection.Column;
			m_packagesContainer.style.overflow = Overflow.Hidden;
			m_packagesContainer.style.flexWrap = Wrap.Wrap;
			m_packagesContainer.style.flexGrow = 1;

			style.borderTopWidth = 1f;
			style.borderLeftWidth = 1f;
			style.borderRightWidth = 1f;
			style.borderBottomWidth = 1f;

			style.borderTopColor = Color.black;
			style.borderLeftColor = Color.black;
			style.borderRightColor = Color.black;
			style.borderBottomColor = Color.black;

			style.minHeight = 100;

			m_busyOverlay = new LFUI.BusyOverlay();

			hierarchy.Add(m_busyOverlay);
		}

		private void OnToolbarBtn1Clicked()
		{
			ToolbarButton1Clicked?.Invoke();
		}

		private void OnToolbarBtn2Clicked()
		{
			ToolbarButton2Clicked?.Invoke();
		}

		public async void ConstructList()
		{
			m_busyOverlay.Display();
			m_packagesContainer.Clear();

			StartExecStopwatch();
			var groupedPackages = m_packageInfos.GroupBy(
				p => p.name.Replace(GOOGLE_PACKAGE_PREFIX, "").Split('.')[0],
				p => p,
				(key, g) => new KeyValuePair<string,List<ScrapedPackageInfo>>(key, g.ToList()));
			foreach (var item in groupedPackages)
			{
				m_packagesContainer.Add(await ConstructGroup(item.Key, item.Value));
			}
			
			UpdatePackagesCountDisplay();
			StopExecStopwatch();
			m_busyOverlay.Hide();
		}

		public void ForceDisplayBusy()
		{
			m_busyOverlay.Display();
		}

		private async Task<VisualElement> ConstructGroup(string groupName, List<ScrapedPackageInfo> scrapedPackageInfos)
		{
			VisualTreeAsset versionTemplate = UI.Helpers.LoadUiVisualTree(SCRAPED_PACKAGE_UXML_FILENAME);
			System.Globalization.TextInfo textInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;

			LFUI.Foldout groupFoldout = new LFUI.Foldout()
			{
				Label = textInfo.ToTitleCase(groupName.Replace("-"," ")),
				Value = false
			};
			groupFoldout.style.marginLeft = 1;
			groupFoldout.style.marginRight = 1;

			groupFoldout.headerContainer.Add(new Label(
				scrapedPackageInfos.Count + " Package" + (scrapedPackageInfos.Count > 1 ? "s" : "")
				));
			for (int i = 0; i < scrapedPackageInfos.Count; i++)
			{
				await YieldIfMaxDurationIsReached();

				ScrapedPackageInfo scrapedPackageInfo = scrapedPackageInfos[i];
				LFUI.Foldout packageFoldout = new LFUI.Foldout
				{
					Label = textInfo.ToTitleCase(
						scrapedPackageInfo.name.Replace(GOOGLE_PACKAGE_PREFIX, "").Replace(".", " ")),
					Value = false
				};

				Label packageNameLabel = new Label(scrapedPackageInfo.name);
				packageNameLabel.style.backgroundColor = new Color(0.5f, 0.5f, 0.5f, 0.25f);
				packageNameLabel.style.marginRight = 0;
				packageNameLabel.style.marginLeft = 0;
				packageNameLabel.style.paddingLeft = 5;
				packageNameLabel.style.paddingRight = 5;
				packageNameLabel.style.borderBottomLeftRadius = 3;
				packageNameLabel.style.borderBottomRightRadius = 3;
				packageNameLabel.style.borderTopLeftRadius = 3;
				packageNameLabel.style.borderTopRightRadius = 3;
				packageFoldout.Add(packageNameLabel);

				int enabledCount = 0;
				Label versionsCount = new Label();
				for (int j = 0; j < scrapedPackageInfo.verions.Count; j++)
				{
					string version = scrapedPackageInfo.verions[j].version;

					VisualElement versionElement = new VisualElement();

					versionTemplate.CloneTree(versionElement);
					versionElement.Q<Label>().text = version;

					Toggle toggle = versionElement.Q<Toggle>();

					string key = scrapedPackageInfo.name + "@" + scrapedPackageInfo.verions[j].version;
					if (m_value != null && m_value.Contains(key))
					{
						toggle.value = true;
						enabledCount++;
					}

					toggle.RegisterValueChangedCallback(v =>
					{
						enabledCount += v.newValue ? 1 : -1;
						versionsCount.text = enabledCount + "/" + scrapedPackageInfo.verions.Count + " Version" + (scrapedPackageInfo.verions.Count > 1 ? "s" : "");
						OnValueChanged(v, key);
					});

					packageFoldout.Add(versionElement);
				}

				versionsCount.text = enabledCount + "/" + scrapedPackageInfo.verions.Count + " Version" + (scrapedPackageInfo.verions.Count > 1 ? "s": "");
				packageFoldout.headerContainer.Add(versionsCount);

				groupFoldout.Add(packageFoldout);
			}
			return groupFoldout;
		}

		private void OnValueChanged(ChangeEvent<bool> value, string key)
		{
			if (value.newValue)
			{
				if (m_value.Contains(key) == false)
				{
					m_value.Add(key);
				}
			}
			else
			{
				m_value.Remove(key);
			}

			UpdatePackagesCountDisplay();

			ValueChanged?.Invoke(key, value.newValue);
		}

		private void UpdatePackagesCountDisplay()
		{
			m_label.text = string.Format(LABEL_TEXT, m_value.Count);
		}

		#region Helpers
		private void StartExecStopwatch()
		{
			m_execStopwatch.Reset();
			m_execStopwatch.Start();
		}

		private void StopExecStopwatch()
		{
			m_execStopwatch.Stop();
		}

		private async Task YieldIfMaxDurationIsReached()
		{
			if (m_execStopwatch.ElapsedMilliseconds > MAX_DURATION_BEFORE_YIELD)
			{
				StopExecStopwatch();
				await Task.Yield();
				StartExecStopwatch();
			}
			return;
		}
		#endregion Helpers

		#endregion Methods
	}
}
