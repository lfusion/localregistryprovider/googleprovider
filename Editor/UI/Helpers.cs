/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider.UI
{
	using UnityEditor;
	using UnityEngine.UIElements;

	public static class Helpers
	{
		#region Contants
		private const string SETTINGS_UI_GUID = "4e5e412a0ab0c8b4e81e8c56a42b973c";

		private static readonly string k_visualTreesPath = AssetDatabase.GUIDToAssetPath(SETTINGS_UI_GUID) + "/VisualTrees/";
		private static readonly string k_stylesPath = AssetDatabase.GUIDToAssetPath(SETTINGS_UI_GUID) + "/Styles/";
		#endregion Constants

		#region Methods
		#region Internal
		internal static StyleSheet LoadUiStyleSheet(string path)
		{
			return AssetDatabase.LoadAssetAtPath<StyleSheet>(k_stylesPath + path);
		}

		internal static VisualTreeAsset LoadUiVisualTree(string path)
		{
			return AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(k_visualTreesPath + path);
		}
		#endregion Internal
		#endregion Methods
	}
}