/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider.UI
{
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine.UIElements;
	using LFLRP = LocalRegistryProvider;
	using LFUI = LocalRegistryProvider.UI;

	// Register a SettingsProvider using UIElements for the drawing framework:
	static class LFusionGoogleProviderSettingsUIElementsRegister
	{
		private const string APPLY_BTN = "Apply now";
		private const string REFRESH_BTN = "Refresh list";

		private static readonly HashSet<string> s_keywords = new HashSet<string>(new string[]
		{
			"LFusion",
			"Local",
			"Registry",
			"Provider",
			"Packages",
			"Google"
		});

		private static PackageListToggle s_packageListToggle = null;
		[SettingsProvider]
		public static SettingsProvider CreateMyCustomSettingsProvider()
		{

			// First parameter is the path in the Settings window.
			// Second parameter is the scope of this setting: it only appears in the Settings window for the Project scope.
			var provider = new SettingsProvider(LFUI.Helpers.SettingsPath(Constants.PackageShortDisplayedName), SettingsScope.Project)
			{
				label = Constants.PackageShortDisplayedName,
				// activateHandler is called when the user clicks on the Settings item in the Settings window.
				activateHandler = (searchContext, rootElement) =>
				{
					VisualElement settingsContainer = LFUI.Helpers.LoadSettingsContainer(rootElement, Constants.PackageDisplayedName, LFLRP.Helpers.GetInstalledPackageVersion(Constants.PackageName));

					Label descriptionLabel = new Label(Constants.Description);
					descriptionLabel.style.marginBottom = 8;
					descriptionLabel.style.whiteSpace = WhiteSpace.Normal;

					settingsContainer.Add(descriptionLabel);

					Foldout mainFoldout = new Foldout() { name = "packagesListContainer", text = "Packages Selector" };
					mainFoldout.AddToClassList("mb-4");
					settingsContainer.Add(mainFoldout);

					var settings = Settings.GetSerialized();

					s_packageListToggle = new PackageListToggle();
					s_packageListToggle.ToolbarButton1Clicked += OnToolbarButton1Clicked;
					s_packageListToggle.ToolbarButton2Clicked += OnToolbarButton2Clicked;
					s_packageListToggle.PackageInfos = GoogleProvider.PackageInfos;
					s_packageListToggle.Value = Settings.instance.EnabledPackages;
					s_packageListToggle.ValueChanged += OnValueChanged;
					s_packageListToggle.ToolbarButton1Text = APPLY_BTN;
					s_packageListToggle.ToolbarButton2Text = REFRESH_BTN;

					if (GoogleProvider.IsRefreshingPackagesList)
					{
						s_packageListToggle.ForceDisplayBusy();
					}
					else
					{
						s_packageListToggle.ConstructList();
					}

					GoogleProvider.IsRefreshingPackagesListChanged += OnIsRefreshingPackagesListChanged;

					mainFoldout.Add(s_packageListToggle);

					Foldout cacheFoldout = new Foldout()
					{
						text = "Cache Manager"
					};
					cacheFoldout.value = false;

					settingsContainer.Add(cacheFoldout);

					cacheFoldout.Add(new TgzProvider.ITgzProviderCacheEditor(ProviderManager.GetProvider<GoogleProvider>().GetPackageSources()));
				},

				deactivateHandler = () =>
				{
					GoogleProvider.IsRefreshingPackagesListChanged -= OnIsRefreshingPackagesListChanged;
					var task = ProviderManager.RefreshProviderAsync<GoogleProvider>();
				},

				// Populate the search keywords to enable smart search filtering and label highlighting:
				keywords = s_keywords
			};

			return provider;
		}

		private static void OnIsRefreshingPackagesListChanged(bool value)
		{
			if (value)
			{
				s_packageListToggle.ForceDisplayBusy();
			}
			else
			{
				s_packageListToggle.ConstructList();
			}
		}

		private static async void OnToolbarButton1Clicked()
		{
			s_packageListToggle.ToolbarButton1Enabled = false;
			await ProviderManager.RefreshProviderAsync<GoogleProvider>();
			s_packageListToggle.ToolbarButton1Enabled = true;
		}

		private static async void OnToolbarButton2Clicked()
		{
			s_packageListToggle.ToolbarButton2Enabled = false;
			await GoogleProvider.RefreshPackagesListAsync();
			s_packageListToggle.ToolbarButton2Enabled = true;
		}

		private static void OnValueChanged(string key, bool value)
		{
			Settings.instance.Save();
			var task = ProviderManager.RefreshProviderAsync<GoogleProvider>();
		}
	}
}