/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider
{
	using HtmlAgilityPack;
	using LFusion.LocalRegistryProvider.TgzProvider;
	using LFusion.LocalRegistryProvider.TgzProvider.Helpers;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Threading.Tasks;
	using UnityEditor;
	using LFUI = LocalRegistryProvider.UI;

	public class GoogleProvider : ITgzProvider
	{
		#region Constants

		private const string LOG_PREFIX = "<color=orange>[Google Provider]</color>";
		private const string SAVE_RELATIVE_PATH = "Library/LFusion/LocalRegisteryProvider/GoogleProviderCache.json";
		private const string GOOGLE_ARCHIVE_URI = "https://developers.google.com/unity/archive";

		private static readonly string s_cachePath = string.Empty;

		#endregion Constants

		#region Fields
		private static List<ScrapedPackageInfo> s_scrapedPackageInfos = new List<ScrapedPackageInfo>();
		private static Task s_refreshPackagesListAsyncTask = null;
		#endregion Fields

		#region Properties
		public override string Name => Constants.PackageDisplayedName;
		public override string Description => Constants.Description;
		public static List<ScrapedPackageInfo> PackageInfos { get => s_scrapedPackageInfos; }
		public static bool IsRefreshingPackagesList { get => s_refreshPackagesListAsyncTask != null; }
		#endregion Properties 

		#region Events
		private static Action<bool> a_isRefreshingPackagesListChanged = null;
		public static event Action<bool> IsRefreshingPackagesListChanged
		{
			add
			{
				a_isRefreshingPackagesListChanged -= value;
				a_isRefreshingPackagesListChanged += value;
			}
			remove
			{
				a_isRefreshingPackagesListChanged -= value;
			}
		}
		#endregion Events

		#region Methods

		#region Constructor
		static GoogleProvider()
		{
			s_cachePath = string.Format("{0}/{1}", Helpers.ProjectPath, SAVE_RELATIVE_PATH);
		}
		#endregion Constructor

		#region ITgzProvider
		/// <summary>
		/// Open Google Provider Settings Panel
		/// </summary>
		public override void OpenSettings()
		{
			SettingsService.OpenProjectSettings(LFUI.Helpers.SettingsPath(Constants.PackageShortDisplayedName));
		}

		/// <summary>
		/// Refresh list of available packages on <see cref="GOOGLE_ARCHIVE_URI"/> & https://github.com/android/tuningfork/releases Asynchronously
		/// </summary>
		/// <returns>Asynchronous Task</returns>
		public static async Task RefreshPackagesListAsync()
		{
			if (s_refreshPackagesListAsyncTask == null)
			{
				s_refreshPackagesListAsyncTask = RefreshPackagesListAsyncInternal();
				TriggerIsRefreshingPackagesListChanged();
			}

			await s_refreshPackagesListAsyncTask;

			if (s_refreshPackagesListAsyncTask != null)
			{
				s_refreshPackagesListAsyncTask = null;
				TriggerIsRefreshingPackagesListChanged();
			}
		}

		protected override void RefreshSources(bool force = false)
		{
			if (s_scrapedPackageInfos.Count == 0 && IsRefreshingPackagesList == false)
			{
				LoadCache();
			}
			LoadCache();
			if (force || s_scrapedPackageInfos.Count == 0)
			{
				ScrapGoogleData();
				RetrievePackagesFromGithub();
				SaveCache();
			}

			UpdateSources();
		}

		protected override async Task RefreshSourcesAsync(bool force = false)
		{
			if (s_scrapedPackageInfos.Count == 0 && IsRefreshingPackagesList == false)
			{
				LoadCache();
			}
			if (force || s_scrapedPackageInfos.Count == 0)
			{
				await RefreshPackagesListAsync();
			}

			UpdateSources();
		}
		#endregion ITgzProvider

		internal static async Task RefreshPackagesListAsyncInternal()
		{
			await ScrapGoogleDataAsync();
			await RetrievePackagesFromGithubAsync();
			SaveCache();
		}

		private void UpdateSources()
		{
			PackageSources.Clear();
			var enabledPackages = Settings.instance.EnabledPackages;
			for (int i = 0; i < s_scrapedPackageInfos.Count; i++)
			{
				for (int j = 0; j < s_scrapedPackageInfos[i].verions.Count; j++)
				{
					string key = s_scrapedPackageInfos[i].name + "@" + s_scrapedPackageInfos[i].verions[j].version;
					if (enabledPackages.Contains(key))
					{
						PackageSources.Add(new PackageSource() { SourceType = SourceType.Http, Path = s_scrapedPackageInfos[i].verions[j].url });
					}
				}
			}
		}

		private static void RetrievePackagesFromGithub()
		{
			Log("Retrieving additional versions of com.google.android.performancetuner from Github release");
			Github.ReleaseVersion[] releaseVersions = Github.GetPackagesFromRelease("android", "tuningfork", x => x.Substring(1));
			InsertGithubDataInScrapedData("com.google.android.performancetuner", releaseVersions);
			Log("Additional versions of com.google.android.performancetuner retrieved from Github release");
		}

		private static async Task RetrievePackagesFromGithubAsync()
		{
			Log("Retrieving additional versions of com.google.android.performancetuner from Github release");
			Github.ReleaseVersion[] releaseVersions = await Github.GetPackagesFromReleaseAsync("android", "tuningfork", x => x.Substring(1));
			InsertGithubDataInScrapedData("com.google.android.performancetuner", releaseVersions);
			Log("Additional versions of com.google.android.performancetuner retrieved from Github release");
		}

		private static void InsertGithubDataInScrapedData(string packageName, Github.ReleaseVersion[] releaseVersions)
		{
			ScrapedPackageInfo packageInfo = s_scrapedPackageInfos.Find(x => x.name == packageName);

			for (int i = 0; i < releaseVersions.Length; i++)
			{
				ScrapedPackageVersionInfo versionInfo = packageInfo.verions.Find(x => x.version == releaseVersions[i].Version);
				if (versionInfo == null)
				{
					packageInfo.verions.Add(new ScrapedPackageVersionInfo() { version = releaseVersions[i].Version, url = releaseVersions[i].Url });
				}
			}

			packageInfo.verions.Sort((x, y) => string.Compare(x.version, y.version) * -1);
		}

		private static void ScrapGoogleData()
		{
			Web.GetResult downloadedValue = Web.Get(GOOGLE_ARCHIVE_URI);

			if (downloadedValue == null)
			{
				return;
			}

			string simplifyedHtml = downloadedValue.Text.Replace("\n", "");

			ParseGoogleData(simplifyedHtml);
		}

		private static async Task ScrapGoogleDataAsync()
		{
			Web.GetResult downloadedValue = await Web.GetAsync(GOOGLE_ARCHIVE_URI);

			if (downloadedValue == null)
			{
				return;
			}

			string simplifyedHtml = downloadedValue.Text.Replace("\n", "");

			await ParseGoogleDataAsync(simplifyedHtml);
		}

		private static void ParseGoogleData(string html)
		{
			s_scrapedPackageInfos.Clear();

			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(html);

			HtmlNodeCollection libsNode = doc.DocumentNode.SelectNodes("//article/div/p/code");

			for (int i = 0; i < libsNode.Count; i++)
			{
				var libNode = libsNode[i];
				var packageInfo = ParseHtmlTable(libNode.InnerHtml, libNode.ParentNode.NextSibling);

				s_scrapedPackageInfos.Add(packageInfo);
			}
		}

		private static async Task ParseGoogleDataAsync(string html)
		{
			s_scrapedPackageInfos.Clear();

			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(html);

			HtmlNodeCollection libsNode = doc.DocumentNode.SelectNodes("//article/div/p/code");

			for (int i = 0; i < libsNode.Count; i++)
			{
				var libNode = libsNode[i];
				var packageInfo = ParseHtmlTable(libNode.InnerHtml, libNode.ParentNode.NextSibling);

				s_scrapedPackageInfos.Add(packageInfo);

				await Task.Yield();
			}
		}

		private static ScrapedPackageInfo ParseHtmlTable(string packageName, HtmlNode tableNode)
		{
			ScrapedPackageInfo packageInfo = new ScrapedPackageInfo();

			packageInfo.name = packageName;

			var lineNodes = tableNode.SelectNodes("tr");

			for (int i = 1; i < lineNodes.Count; i++)
			{
				var row = lineNodes[i];

				var cells = row.SelectNodes("td");

				if (cells.Count == 5)
				{
					//Version = new Version(cells[0].InnerText);
					//ReleaseDate = DateTime.Parse(cells[1].InnerText);

					var urlsNode = cells[3].SelectNodes(".//a");

					foreach (var urlNode in urlsNode)
					{
						var url = urlNode.GetAttributeValue("href", string.Empty);

						if (url.Contains(".tgz"))
						{
							//packageInfo.AddVersion(cells[0].InnerText, new AuthorInfo() { Name = "Google LLC" }, new DistInfo(url, ""));
							packageInfo.verions.Add(new ScrapedPackageVersionInfo() { version = cells[0].InnerText, url = url });
							break;
						}
					}

				}
			}

			return packageInfo;
		}

		private static void SaveCache()
		{
			Directory.CreateDirectory(Directory.GetParent(s_cachePath).ToString());
			File.WriteAllText(s_cachePath, s_scrapedPackageInfos.ToJson());
		}

		private static void LoadCache()
		{
			if (!File.Exists(s_cachePath))
			{
				return;
			}
			string json = File.ReadAllText(s_cachePath);

			s_scrapedPackageInfos.ParseJson(json);
		}

		private static void TriggerIsRefreshingPackagesListChanged(bool? value = null)
		{
			if (value == null)
			{
				value = IsRefreshingPackagesList;
			}

			try
			{
				a_isRefreshingPackagesListChanged?.Invoke((bool)value);
			}
			catch (Exception e)
			{
				LogError(e.Message + "\n" + e.StackTrace);
			}
		}

		#region Debugs

		private static void Log(string log)
		{
			LocalRegistryProvider.Debug.Log(log, LOG_PREFIX);
		}

		private static void LogWarning(string log)
		{
			LocalRegistryProvider.Debug.LogWarning(log, LOG_PREFIX);
		}

		private static void LogError(string log)
		{
			LocalRegistryProvider.Debug.LogError(log, LOG_PREFIX);
		}

		#endregion Debugs
		#endregion Methods
	}
}
