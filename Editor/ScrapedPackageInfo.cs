/*
*	LFusion Google Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference Google packages.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.GoogleProvider
{
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	[Serializable]
	public class ScrapedPackageInfo : IToJson, IParseJson
	{
		[SerializeField] public string name;
		[SerializeField] public List<ScrapedPackageVersionInfo> verions = new List<ScrapedPackageVersionInfo>();

		public void ParseJson(string json)
		{
			JsonUtility.FromJsonOverwrite(json, this);
		}

		public string ToJson(bool pretty = false)
		{
			return JsonUtility.ToJson(this, pretty);
		}

		public override string ToString()
		{
			string str = name;
			for (int i = 0; i < verions.Count; i++)
			{
				str += "\n - " + verions[i].version;
			}
			return str;
		}
	}

	[Serializable]
	public class ScrapedPackageVersionInfo
	{
		[SerializeField] public string version;
		[SerializeField] public string url;
	}
}
