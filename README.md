﻿# LFusion Google Provider

[[English]](/package/Documentation~/Google%20Provider%20EN.md) 
[[Français]](/package/Documentation~/Google%20Provider%20FR.md)

Ce package est une extension pour LFusion Local Registry Provider.  
Il permet de référencer les packages disponibles sur:  
 - https://developers.google.com/unity/archive
 - https://github.com/android/tuningfork/releases

 1. [Prérequis](#prérequis)
 2. [Installation](#installation)
 3. [Configuration](#configuration)

## Prérequis

 - ***Unity 2020*** ou supérieur
 - ***LFusion Local Registry Provider*** : `com.lfusion.localregistryprovider.core`
 - ***LFusion Tgz Provider*** : `com.lfusion.localregistryprovider.tgzprovider`

## Installation

### Installer via Gitlab registry

 - Ajouter le registre [LFusion Upm-Registry](https://gitlab.com/lfusion/upm-registry)
 - **Pour Unity 2021 ou plus récent**
	 - Package Manager
	 - **\+** Add package by name...
		 - `Name : com.lfusion.localregistryprovider.googleprovider`
 - **Pour toutes les versions supportées**
	 - Ouvrir `manifest.json` dans le dossier Packages du projet.
	 - Ajouter `"com.lfusion.localregistryprovider.googleprovider": "x.x.x",` dans le bloc `dependencies`  en remplaçant `x.x.x` par la version voulu.
	 - Exemple pour la version `1.0.0` :
```json
{
  "dependencies": {
    "com.lfusion.localregistryprovider.googleprovider": "1.0.0",
    [...]
  },
  [...]
}
```

### Installer via tarball

 - Télécharger `com.lfusion.localregistryprovider.googleprovider-x.x.x.tgz` depuis [releases](https://gitlab.com/lfusion/localregistryprovider/googleprovider/-/releases).
 - Package Manager
 - **\+** Add package from tarball...
 - Sélectionnez  `com.lfusion.localregistryprovider.googleprovider-x.x.x.tgz`

## Configuration

Les paramètres de LFusion Google Provider sont accessibles depuis:  

 1. `Edit/Project Settings...`
 2. `Package Manager/Local Registry/Google Provider`

### Packages Selector

Liste les packages google disponible et permet de sélectionner ceux que vous souhaitez utiliser dans le projet.

| Nom                   | Description                                           |
|-----------------------|-------------------------------------------------------|
| Apply Now             | Force la mise à jour des packages listés              |
| Refresh List          | Récupère la liste des packages disponibles            |

### Cache Manager

Permet de supprimer les informations des packages stockés en cache.  