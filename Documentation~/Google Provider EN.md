﻿# LFusion Google Provider

This package is an extension for LFusion Local Registry Provider.  
It can be used to reference packages available on:  
 - https://developers.google.com/unity/archive
 - https://github.com/android/tuningfork/releases

 1. [Prerequisites](#prerequisites)
 2. [Installation](#installation)
 3. [Configuration](#configuration)

## Prerequisites

 - ***Unity 2020*** or higher
 - ***LFusion Local Registry Provider*** : `com.lfusion.localregistryprovider.core`
 - ***LFusion Tgz Provider*** : `com.lfusion.localregistryprovider.tgzprovider`

## Installation

### Install via Gitlab registry

 - Add the [LFusion Upm-Registry](https://gitlab.com/lfusion/upm-registry)
 - **For Unity 2021 or highter**
	 - Package Manager
	 - **\+** Add package by name...
		 - `Name : com.lfusion.localregistryprovider.googleprovider`
 - **For all supported Unity versions**
	 - Open `manifest.json` in the Packages folder of the project.
	 - Add `"com.lfusion.localregistryprovider.googleprovider": "x.x.x",` to the `dependencies` block, replacing `x.x.x` with the desired version.
	 - Example for version `1.0.0` :
```json
{
  "dependencies": {
    "com.lfusion.localregistryprovider.googleprovider": "1.0.0",
    [...]
  },
  [...]
}
```

### Install via tarball

 - Download `com.lfusion.localregistryprovider.googleprovider-x.x.x.tgz` from [releases](https://gitlab.com/lfusion/localregistryprovider/googleprovider/-/releases).
 - Package Manager
 - **\+** Add package from tarball...
 - Select  `com.lfusion.localregistryprovider.googleprovider-x.x.x.tgz`

## Configuration

LFusion Google Provider settings can be accessed from:  

 1. `Edit/Project Settings...`
 2. `Package Manager/Local Registry/Google Provider`

### Packages Selector

It lists the google packages available and allows you to select the ones you want to use in the project.

| Nom                   | Description                                           |
|-----------------------|-------------------------------------------------------|
| Apply Now             | Force la mise à jour des packages listés              |
| Refresh List          | Récupère la liste des packages disponibles            |

### Cache Manager

Used to delete cached package information.  